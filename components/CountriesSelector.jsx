import { useStats } from "../utils/useStats";
export function CountriesSelector() {
    const countries = useStats("https://covid19.mathdro.id/api/countries");
    console.log(countries);
  if (!countries) return <p>Loading...</p>;
  return (
    <div>
      <select>
        {countries.countries.map((country) => (
          <option key={country.iso3} value={country.iso3}>
            {country.name}
          </option>
        ))}
      </select>
    </div>
  );
}
