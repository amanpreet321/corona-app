import { Stats } from '../components/Stats';
import { CountriesSelector } from '../components/CountriesSelector';

export default function IndexPage() {
    return (<>
        <Stats></Stats>
        <CountriesSelector></CountriesSelector>
    </>);
}